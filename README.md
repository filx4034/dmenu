# My dmenu fork
This is myy version of the [dmenu launcher](https://tools.suckless.org/dmenu/)  
I used [dmenu flexipatch](https://github.com/bakkeby/dmenu-flexipatch) because it is easier

## Patches
Here's all the patches applied

+ case-insensitive
+ ctrl+v-to-paste
+ highlight
+ instant
+ line-height
+ numbers
